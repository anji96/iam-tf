provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "/Users/vxxxxg/.aws/credentials"
  profile                 = "personal_user"
}

resource "aws_iam_role" "toolchain-role" {
  name  = var.role_name
  assume_role_policy   = data.aws_iam_policy_document.assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "tc-role-policy-attach" {
  count      = "${length(var.managed_policies)}"
  policy_arn = "${element(var.managed_policies, count.index)}"
  role       = "${aws_iam_role.toolchain-role.name}"
}

data "aws_iam_policy_document" "assume_role_policy" {

  statement {
    sid    = "assumeRole"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::xxxxxxxxxxxx:root"]
    }

    actions = ["sts:AssumeRole"]

  }
}
